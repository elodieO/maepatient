//
//  connectionManager.swift
//  MaePatient
//
//  Created by Chloe David on 01/03/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import SocketIO
import UIKit

class SocketConnectionManager :NSObject {
    
    static let shared = SocketConnectionManager()
    
    var patientStatus: Bool = false
    var familyStatus: Bool = false
    var everyoneConnected: Bool = false
    var batteryLevel:((String)->())? = nil
    
    func returnConnections(familyImage : UIImageView, patientImage : UIImageView){
        
        SocketIOManager.shared.didIConnectCallback = { (bool) -> () in
            if(bool){
                self.patientStatus = true
            }else{
                self.everyoneConnected = false
                self.patientStatus = false
            }
            self.showConnectionStatus(familyImage: familyImage, patientImage: patientImage)
        }
        
        SocketIOManager.shared.didFamilyConnectCallback = { (bool) -> () in
            if bool {
                self.everyoneConnected = true
                self.familyStatus = true
            }else{
                self.everyoneConnected = false
                self.familyStatus = false
                SocketIOManager.shared.allMemberConnectionCallback?(false)
            }
            self.showConnectionStatus(familyImage: familyImage, patientImage: patientImage)
        }
        
        SocketIOManager.shared.allMemberConnectionCallback = { (bool) -> () in
            if bool {
                self.everyoneConnected = true
                self.familyStatus = true
            }else{
                self.everyoneConnected = false
            }
            self.showConnectionStatus(familyImage: familyImage, patientImage: patientImage)
        }
        
        SocketIOManager.shared.batteryLevel = { (pourcentage) -> () in
            print(pourcentage)
            self.batteryLevel?(pourcentage)
        }
        
        self.showConnectionStatus(familyImage: familyImage, patientImage: patientImage)
        
        
    }
    
    func showConnectionStatus(familyImage : UIImageView, patientImage : UIImageView){
        if self.patientStatus {
            patientImage.image = UIImage(named: "patientPictoConnected")
        }else{
            patientImage.image = UIImage(named: "patientPicto")
        }
        
        if self.familyStatus {
            familyImage.image = UIImage(named: "famillePictoConnected")
        }else{
            familyImage.image = UIImage(named: "famillePicto")
        }
    }
    
}
