//
//  SocketIOManager.swift
//  Anselme
//
//  Created by digital on 10/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import SocketIO

class SocketIOManager :NSObject {
    
    static let shared = SocketIOManager()
    
    var socket: SocketIOClient!
    let manager = SocketManager(socketURL: URL(string: "https://mae.suriteka.website")!, config: [.log(false), .compress, .forcePolling(true), .forceNew(true)])
    
    var didReceiveDataCallback:((Data)->())? = nil
    
    var didIConnectCallback:((Bool)->())? = nil
    var didFamilyConnectCallback:((Bool)->())? = nil
    var allMemberConnectionCallback:((Bool)->())? = nil
    
    var receiveInvitationCallback:(()->())? = nil
    var famillyAnswerInvitationCallback:((Bool)->())? = nil
    
    var didBleConnectedCallback:((Bool)->())? = nil
    var didSparkConnectedCallback:((Bool)->())? = nil
    
    var nextStepCallback:((String)->())? = nil
    
    var feelingFinished:(()->())? = nil
    var feelingReceived:((String)->())? = nil
    
    var sparkMotorsAreOffCallback:(()->())? = nil
    
    var didIAmPiloting:((Bool)->())? = nil
    
    var experienceEnded:(()->())? = nil
    var droneLanded:(()->())? = nil
    
    var msgReceived:((String)->())? = nil
    
    var batteryLevel:((String) -> ())? = nil
    
    override init() {
        super.init()
        socket = manager.defaultSocket
    }
    
    func connectSocket() {
        socket.on("connect") { _, _ in
            
            self.socket.emit("connectedPatient")
            
            self.socket.on("connectedPatientCallback"){ (dataArray, ack) in
                print(dataArray[0])
                self.didIConnectCallback?(true)
            }
            
            self.socket.on("toVideo") { (dataArray, ack) in
                if let videoData = dataArray[0] as? Data {
                    self.didReceiveDataCallback?(videoData)
                }
            }
            
            self.socket.on("allMembersConnected"){ (dataArray, ack) in
                print(dataArray[0])
                self.allMemberConnectionCallback?(true)
            }
            
            self.socket.on("disconnectedFamily"){ (dataArray, ack) in
                print(dataArray[0])
                self.didFamilyConnectCallback?(false)
                self.allMemberConnectionCallback?(false)
            }
            
            self.socket.on("nextStep"){ (dataArray, ack) in
                if let step = dataArray[0] as? String {
                    self.nextStepCallback?(step)
                }
            }
            
            self.socket.on("feelingFinished"){(dataArray, ack) in
                self.feelingFinished?()
            }
            
            self.socket.on("actionSparkFeeling"){(dataArray, ack) in
                if let feeling = dataArray[0] as? String {
                    self.feelingReceived?(feeling)
                }
                
            }
        
            self.socket.on("invitationReceived"){ (dataArray, ack) in
                print("invitation received")
                self.receiveInvitationCallback?()
            }

            self.socket.on("invitationAnswered"){ (dataArray, ack) in
                print("invitation answer : \(dataArray[0])")
                if dataArray[0] as? String == "Yes" {
                    self.socket.emit("nextStep", "1")
                } else {
                    SocketIOManager.shared.famillyAnswerInvitationCallback?(false)
                }
            }

            self.socket.on("bleConnected"){(dataArray, ack) in
                print("Ble Connected")
                self.didBleConnectedCallback?(true)
            }

            self.socket.on("sparkConnected"){(dataArray, ack) in
                print("Spark Connected")
                self.didSparkConnectedCallback?(true)
            }
            
            self.socket.on("sparkMotorsOff"){(dataArray, ack) in
                print("spark motor's off")
                self.sparkMotorsAreOffCallback?()
            }
            
            self.socket.on("familyWatching"){(dataArray, ack) in
                print("family is watching")
                self.didIAmPiloting?(true)
            }

            self.socket.on("familyPiloting"){(dataArray, ack) in
                print("family is piloting")
                self.didIAmPiloting?(false)
            }
            
            self.socket.on("droneLanded"){(dataArray, ack) in
                print("landing")
                self.droneLanded?()
            }
            
            self.socket.on("endExperience"){(dataArray, ack) in
                print("end experience")
                self.experienceEnded?()
            }
            
            self.socket.on("sendMsgToPatient"){(dataArray, ack) in
                if let msg = dataArray[0] as? String {
                    print("new msg : \(msg)")
                    self.msgReceived?(msg)
                }
            }
            self.socket.on("sendLevelBattery"){(dataArray, ack) in
                if let battery = dataArray[0] as? String {
                    self.batteryLevel?(battery)
                }
            }
            
        }
        
        socket.on("disconnect") { _,_ in
            
        }
        
        socket.connect()
        
    }
    
    func sendMsg(msg: String) {
        print("send msg")
        self.socket.emit("sendMsgToFamily", msg)
    }
    
    func actionSparkEvent(event:String) {
        print(event)
        self.socket.emit("actionSparkEvent", event)
    }
    
    func actionSparkDirectionHorizontal(direction:String) {
        print("pilotage \(direction)")
        self.socket.emit("actionSparkDirectionHorizontal", direction)
    }
    
    func actionSparkRotation(direction:String) {
        print("pilotage \(direction)")
        self.socket.emit("actionSparkRotation", direction)
    }
    
    func actionSparkFeeling(feelingName:String) {
        print(feelingName)
        self.socket.emit("actionSparkFeeling", feelingName)
    }
    
    func sendInvitation() {
        print("send invitation")
        self.socket.emit("invitationSentToFamily", "patient invite you to start the experience")
    }
    
    func answerInvitation(answer:String, msg:String) {
        print("answer invitation")
        if msg != "" {
            print("I send msg \(msg)")
            self.socket.emit("sendMsgToFamily", msg)
        }
        self.socket.emit("patientAnswered", answer)
    }
    
    func patientReady(){
        print("patient ready")
        self.socket.emit("patientReadyToTakeOff", "Patient ready to take off")
    }
    
    func endExperience(){
        print("end")
        self.socket.emit("endExperience")
    }
    
    func sendGimbalValue(y: Float){
        print("send gimbal value")
        self.socket.emit("sendGimbalValue", "\(y)")
    }
    
}
