//
//  UIView.swift
//  MaePatient
//
//  Created by Chloe David on 26/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

extension UIView {
    func roundedCorner() {
        self.layer.cornerRadius = self.layer.frame.height / 2.0
        self.clipsToBounds = true
    }
    
    func makeVertical()
    {
        transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 2))
    }
    
    func buttonShadow() {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 10.0
        self.layer.masksToBounds = false
    }
    
    func backgroundGradient(color: String) {
        let customLayer = CAGradientLayer()
        customLayer.frame = self.bounds
        
        if color == "blue" {
            customLayer.colors = [UIColor.clear.cgColor, UIColor(red:0.09, green:0.81, blue:0.88, alpha:0.7).cgColor]
        } else if color == "red" {
            customLayer.colors = [UIColor.clear.cgColor, UIColor(red:1.00, green:0.00, blue:0.00, alpha:1.0).cgColor]
        } else if color == "redGradient" {
            customLayer.colors = [UIColor(red:1.00, green:0.00, blue:0.00, alpha:0.5).cgColor, UIColor(red:0.90, green:0.01, blue:0.73, alpha:0.5).cgColor]
        } else if color == "greyYellow" {
            customLayer.colors = [UIColor(red:1.00, green:1.00, blue:0.99, alpha:0.5).cgColor,UIColor(red:1.00, green:0.97, blue:0.78, alpha:0.5).cgColor]
        }
        
        customLayer.startPoint = CGPoint(x: 0, y: 0)
        customLayer.endPoint = CGPoint(x: 1, y: 0)
        self.layer.addSublayer(customLayer)
    
    }
}

extension UIFont {
    
}

extension UIButton {
    func buttonOrange() {
        
        let customLayer = CAGradientLayer()
        customLayer.frame = self.bounds
        customLayer.colors = [UIColor(red:1.00, green:0.73, blue:0.00, alpha:1.0).cgColor, UIColor(red:1.00, green:0.23, blue:0.00, alpha:1.0).cgColor]
        customLayer.startPoint = CGPoint(x: 0, y: 0)
        customLayer.endPoint = CGPoint(x: 1, y: 0)
        self.layer.addSublayer(customLayer)
        
        self.frame.size = CGSize(width: 250, height: 48)
        
        self.setTitleColor(UIColor.white, for: .normal)
        self.titleLabel!.font = UIFont(name: "Nexa Light", size: 17)
        self.contentHorizontalAlignment = contentHorizontalAlignment;
        
    }
}


extension UISlider {
    func customSlider() {
        self.roundedCorner()
        
        self.tintColor = UIColor.clear
        self.maximumTrackTintColor = UIColor.clear
        self.minimumTrackTintColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
    }
    
    func customSliderInvitation() {
        self.customSlider()
        self.setThumbImage(#imageLiteral(resourceName: "el_sendSlider"), for: .normal)
        self.frame.size = CGSize(width: 250, height: 72)
    }
    
    func customSliderTakeOff() {
        self.customSlider()
        self.setThumbImage(#imageLiteral(resourceName: "el_takeOffSliderTint"), for: .normal)
        self.frame.size = CGSize(width: 186, height: 72)
    }
    
    func customSliderGimbal() {
        self.customSlider()
        self.setThumbImage(#imageLiteral(resourceName: "el_gimbal"), for: .normal)
        self.frame.size = CGSize(width: 126, height: 74)
    }
}

