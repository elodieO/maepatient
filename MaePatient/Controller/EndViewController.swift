//
//  EndViewController.swift
//  MaePatient
//
//  Created by Chloe David on 02/03/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class EndViewController: UIViewController {
    
    @IBOutlet weak var familyImage: UIImageView!
    @IBOutlet weak var patientImage: UIImageView!
    
    @IBOutlet weak var closeApp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //////////// TOP BAR ////////////
        
        SocketConnectionManager.shared.returnConnections(familyImage: self.familyImage, patientImage: self.patientImage)
        
        
        //////////// STYLE ////////////
        
        self.closeApp.buttonOrange()
        self.closeApp.roundedCorner()

    }
    
    @IBAction func closeApp(_ sender: Any) {
        self.performSegue(withIdentifier: "goBackHome", sender: nil)
        UIControl().sendAction(#selector(NSXPCConnection.suspend),
                               to: UIApplication.shared, for: nil)
    }
    
    @IBAction func goBackToHome(_ sender: Any) {
        self.performSegue(withIdentifier: "goBackHome", sender: nil)
    }
    
}
