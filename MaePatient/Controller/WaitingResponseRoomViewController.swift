//
//  WaitingResponseRoomViewController.swift
//  MaePatient
//
//  Created by Chloe David on 21/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class WaitingResponseRoomViewController: UIViewController {

    @IBOutlet weak var familyImage: UIImageView!
    @IBOutlet weak var patientImage: UIImageView!
    
    var callback: ((Bool) -> ())? = nil
    var message: ((String) -> ())? = nil
    var myTimer: Timer? = nil
    let messages : [String] = [
        "Ils ne devraient plus tarder",
        "Attendons encore un peu",
        "Attendons leur réponse ensemble !"]
    var index:Int = 0
    
    @IBOutlet weak var waitingMessage: UILabel!
    @IBOutlet weak var illustration: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         SocketConnectionManager.shared.returnConnections(familyImage: self.familyImage, patientImage: self.patientImage)
        
        self.illustration.image = UIImage(named: "fond_std")

        SocketIOManager.shared.nextStepCallback = {(step) -> () in
            if step == "1" {
                self.performSegue(withIdentifier: "goToInstallProcedureAfterWait", sender: nil)
            }
        }
        
        SocketIOManager.shared.famillyAnswerInvitationCallback = { (bool) -> () in
            if !bool  {
                print("invitation refused")
                self.callback?(false)
                _ = self.navigationController?.popViewController(animated: true)
            
            }
        }
        
        SocketIOManager.shared.msgReceived = {(msg) -> () in
            print("invitation refused with message")
                self.message?(msg)
                _ = self.navigationController?.popViewController(animated: true)
            
        }
        
        
        myTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(6.0), repeats: true){
            (t) in
            
            if (self.index < self.messages.count){
                UIView.animate(withDuration: 2.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.waitingMessage.alpha = 0.0
                }, completion: nil)
                
                self.waitingMessage.text = self.messages[self.index]
                
                UIView.animate(withDuration: 2.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.waitingMessage.alpha = 1.0
                }, completion: nil)
                
                self.index += 1
            } else {
                //self.updateTimer(timer: self.myTimer!)
                self.index = 0
            }
        }
    }
    

    func updateTimer(timer: Timer){
        timer.invalidate()
    }

}
