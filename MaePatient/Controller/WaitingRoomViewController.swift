//
//  WaitingRoomViewController.swift
//  MaePatient
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class WaitingRoomViewController: UIViewController {

    
    @IBOutlet weak var illustration: UIImageView!
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var sendInvitation: UIButton!
    @IBOutlet weak var sendBack: UIButton!
    @IBOutlet weak var invitationStatus: UILabel!
    @IBOutlet weak var goBack: UIButton!
    
    // Messages
    @IBOutlet weak var message1: UIView!
    @IBOutlet weak var message2: UIView!
    @IBOutlet weak var message3: UIView!
    @IBOutlet weak var messagePop: UIView!
    @IBOutlet weak var messagePopImg: UIImageView!
    
    @IBOutlet weak var familyImage: UIImageView!
    @IBOutlet weak var patientImage: UIImageView!
    
    @IBOutlet weak var invitationView: UIView!
    @IBOutlet weak var answerInvitationview: UIView!
    @IBOutlet weak var receiveBadResponseView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Connect to the server
        SocketIOManager.shared.connectSocket()
        SocketConnectionManager.shared.returnConnections(familyImage: self.familyImage, patientImage: self.patientImage)
        
        // Set invitation view on loading
        self.invitationViewActived()
        
        // Button style
        self.sendInvitation.buttonOrange()
        self.sendBack.buttonOrange()
        self.sendInvitation.roundedCorner()
        self.sendBack.roundedCorner()
        
        // Slider style
        self.slider.customSliderInvitation()
        self.slider.value = 0.5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        ////////// SOCKET - RECEIVING INVITATION //////////
        
        SocketIOManager.shared.receiveInvitationCallback = { () -> () in
            self.receivingInvitationViewActived()
        }
        
        ////////// SOCKET - GO TO INSTALLATION PROCEDURE //////////
        
        SocketIOManager.shared.nextStepCallback = {(step) -> () in
            if step == "1" {
                self.performSegue(withIdentifier: "goToInstallProcedure", sender: nil)
            }
        }
    }
    
    
    ////////// BAD RESPONSE RECEIVED FROM WAITING ROOM //////////
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToWaitingResponseRoom"{
            let waitingRoomVC = segue.destination as! WaitingResponseRoomViewController
            waitingRoomVC.callback = { result in
                if !result{
                    print("Negative response")
                    self.receivingBadResponseViewActivated()
                }
            }
            waitingRoomVC.message = { msg in
                print("callback : \(msg)")
                self.receivingBadResponseViewActivated()
                self.messagePop.isHidden = false
                if msg == "message1" {
                    self.messagePopImg.image = #imageLiteral(resourceName: "messagef1")
                } else if msg == "message2" {
                    self.messagePopImg.image = #imageLiteral(resourceName: "messagef2")
                }
            }
        }
    }
    
    
    ////////// GO BACK TO SEND INVITATION VIEW //////////
    
    @IBAction func goBackClicked(_ sender: Any) {
        self.invitationViewActived()
    }
    
    
    ////////// SEND AN INVITATION //////////
    
    @IBAction func sendInvitation(_ sender: Any) {
        SocketIOManager.shared.sendInvitation()
        self.performSegue(withIdentifier: "goToWaitingResponseRoom", sender: nil)
        
    }
    
    
    ////////// ANSWER THE FAMILY INVITATION //////////
    
    @IBAction func answerInvitation(_ sender: Any) {
        if self.slider.value == self.slider.maximumValue {
            self.slider.isEnabled = false
            SocketIOManager.shared.answerInvitation(answer: "Yes", msg: "")
        } else if self.slider.value == self.slider.minimumValue {
            SocketIOManager.shared.answerInvitation(answer: "No", msg: "")
            self.invitationViewActived()
        }
    }
    
    
    ////////// REFUSE MESSAGES //////////
    
    @IBAction func message1(_ sender: Any) {
        SocketIOManager.shared.answerInvitation(answer: "No", msg: "message1")
        self.invitationViewActived()
    }
    
    @IBAction func message2(_ sender: Any) {
        SocketIOManager.shared.answerInvitation(answer: "No", msg: "message2")
        self.invitationViewActived()
    }
    
    @IBAction func message3(_ sender: Any) {
        SocketIOManager.shared.answerInvitation(answer: "No", msg: "message3")
        self.invitationViewActived()
    }
    
    // Close message
    @IBAction func tapMsg(_ sender: Any) {
        self.messagePop.isHidden = true
    }
    
    
    ////////// INVITATION VIEW ACTIVATED //////////
    
    func invitationViewActived() {
        self.invitationView.isHidden = false
        self.answerInvitationview.isHidden = true
        self.receiveBadResponseView.isHidden = true
        
        self.messagePop.isHidden = true
        
        self.illustration.image = UIImage(named: "fond_invitation_1")
    }
    
    
    ////////// RECEIVED INVITATION ACTIVATED //////////
    
    func receivingInvitationViewActived() {
        self.answerInvitationview.isHidden = false
        self.invitationView.isHidden = true
        self.receiveBadResponseView.isHidden = true
        
        self.messagePop.isHidden = true
        self.slider.value = 0.5
        
        self.illustration.image = UIImage(named: "fond_invitation_2")
    }
    
    
    ////////// BAD RESPONSE ACTIVATED //////////
 
    func receivingBadResponseViewActivated(){
        self.receiveBadResponseView.isHidden = false
        self.answerInvitationview.isHidden = true
        self.invitationView.isHidden = true
        
        self.messagePop.isHidden = true
        
        self.illustration.image = UIImage(named: "fond_invitation_3")
    }
}
