//
//  FeelingViewCell.swift
//  MaePatient
//
//  Created by Chloe David on 24/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class FeelingViewCell: UICollectionViewCell {
 
    @IBOutlet weak var icon: UIImageView!
    
}
