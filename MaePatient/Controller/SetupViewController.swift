//
//  SetupViewController.swift
//  MaePatient
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class SetupViewController: UIViewController {
    
    @IBOutlet weak var dots: UIImageView!
    @IBOutlet weak var msgTitle: UILabel!
    @IBOutlet weak var connectionType: UILabel!
    @IBOutlet weak var msgPart1: UILabel!
    @IBOutlet weak var msgPart2: UILabel!
    @IBOutlet weak var illustration: UIImageView!
    
    @IBOutlet weak var familyImage: UIImageView!
    @IBOutlet weak var patientImage: UIImageView!
    
    
    let messages : [[Any]] = [
        [
            "Pour un vol en toute quiétude",
            "Prends le temps de bien t'installer,",
            "et de te souvenir de ces conseils.",
            "Votre vaisseau se met en route",
            #imageLiteral(resourceName: "el_dots_1")
        ],
        [
            "En cas de souci, songe à l'arrêt d'urgence",
            "Clique 3 fois sur l'écran",
            "pour couper les moteurs.",
            "Nous préparons les expressions lumineuses",
            #imageLiteral(resourceName: "el_dots_2")
        ],
        [
            "Vole en toute sécurité avec les autres",
            "Prends ton envol et sois libre de tes mouvements,",
            "mais attention à ton environnement.",
            "Préparation au décollage",
            #imageLiteral(resourceName: "el_dots_3")
        ]
    ]
    
    var bleConnectionSet:Bool = false
    var sparkConnectionSet:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.connectionToDronePhase()
        
        SocketConnectionManager.shared.returnConnections(familyImage: self.familyImage, patientImage: self.patientImage)

        SocketIOManager.shared.didBleConnectedCallback = { (bool) -> () in
            self.LastMessagePhase()
        }

        SocketIOManager.shared.didSparkConnectedCallback = { (bool) -> () in
            self.connectionToBlePhase()
        }
        
        SocketIOManager.shared.nextStepCallback = {(step)->() in
            if step == "2" {
                self.performSegue(withIdentifier: "goToControl", sender: nil)
            }
        }
    }
    
    func messagesAnimation(array: Array<Any>, nb: Int){
        UIView.animate(withDuration: 2.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.msgTitle.alpha = 0.0
            self.msgPart1.alpha = 0.0
            self.msgPart2.alpha = 0.0
            self.connectionType.alpha = 0.0
            self.dots.alpha = 0.0
            self.illustration.alpha = 0.0
        }, completion: nil)
        
        self.msgTitle.text = array[0] as? String
        self.msgPart1.text = array[1] as? String
        self.msgPart2.text = array[2] as? String
        self.connectionType.text = array[3] as? String
        self.dots.image = array[4] as? UIImage
        self.illustration.image = UIImage(named: "fond_setup_\(nb)")
        
        UIView.animate(withDuration: 2.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.msgTitle.alpha = 1.0
            self.msgPart1.alpha = 1.0
            self.msgPart2.alpha = 1.0
            self.connectionType.alpha = 1.0
            self.dots.alpha = 1.0
            self.illustration.alpha = 1.0
        }, completion: nil)
    }
    
    func connectionToDronePhase(){
        let connectionToDroneMessage: Array<Any> = self.messages[0]
        self.messagesAnimation(array: connectionToDroneMessage, nb: 1)
    }
    
    func connectionToBlePhase(){
        let connectionToBleMessage: Array<Any> = self.messages[1]
        self.messagesAnimation(array: connectionToBleMessage, nb: 2)
    }
    
    func LastMessagePhase(){
        let LastMessage: Array<Any> = self.messages[2]
        self.messagesAnimation(array: LastMessage, nb: 3)
    }
}
