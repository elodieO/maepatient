//
//  ControlViewController.swift
//  MaePatient
//
//  Created by Chloe David on 17/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit
import VideoPreviewer

class ControlViewController: UIViewController {
    
    let prev = VideoPreviewer()
    var feelingIsPlaying:Bool = false
    var feelingsList:Array = [
        ["yes", #imageLiteral(resourceName: "icon_yes")],
        ["no", #imageLiteral(resourceName: "icon_no")],
        ["understood", #imageLiteral(resourceName: "icon_understood")],
        ["reflecting", #imageLiteral(resourceName: "icon_reflecting")],
        ["repeating", #imageLiteral(resourceName: "icon_repeating")],
        ["smilingYellow", #imageLiteral(resourceName: "icon_smiling")],
        ["heartPink", #imageLiteral(resourceName: "icon_heart")],
        ["happyRainbow",#imageLiteral(resourceName: "icon_happy")],
        ["tiredPurple", #imageLiteral(resourceName: "icon_tired")],
        ["sadBlue", #imageLiteral(resourceName: "icon_sad")],
        ["angryRed", #imageLiteral(resourceName: "icon_angry")],
        ["leaving", #imageLiteral(resourceName: "icon_leaving")],
        ["followMe", #imageLiteral(resourceName: "icon_folowMe")],
        ["lookAtMe", #imageLiteral(resourceName: "icon_lookAtMe")]
    ]

    @IBOutlet weak var videoPreview: UIView!
    
    // Take Off Phase
    @IBOutlet weak var takeOffSlider: UISlider!
    @IBOutlet weak var validatedTakeOffLabel: UILabel!
    @IBOutlet weak var endTakeOff: UIImageView!
    @IBOutlet weak var endTakeOffView: UIView!
    @IBOutlet weak var takeOffView: UIView!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var waitingMessage1: UILabel!
    @IBOutlet weak var waitingMessage2: UILabel!
    
    // Feelings
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var popFeeling: UIView!
    @IBOutlet weak var popFeelingImg: UIImageView!
    @IBOutlet weak var wiz: UIView!
    @IBOutlet weak var feelingView: UIView!
    
    // Stop Emmergency Phase
    @IBOutlet weak var stopMotorLabel: UILabel!
    @IBOutlet weak var stopMotorYes: UIButton!
    @IBOutlet weak var stopMotorNo: UIButton!
    @IBOutlet weak var stopMotorsView: UIView!
    @IBOutlet weak var viewBg2: UIView!
    
    // Piloting
    @IBOutlet weak var forward: UIView!
    @IBOutlet weak var left: UIView!
    @IBOutlet weak var right: UIView!
    @IBOutlet weak var gimbal: UIView!
    @IBOutlet weak var gimbalSlider: UISlider!
    @IBOutlet weak var commandsView: UIView!
    @IBOutlet weak var endCommands: UIView!
    @IBOutlet weak var viewBgCommands: UIView!
    @IBOutlet weak var batteryMsg30: UIView!
    @IBOutlet weak var batteryMsg25: UILabel!
    @IBOutlet weak var pilotageMsg: UILabel!
    @IBOutlet weak var feelingBg: UIImageView!
    
    // Top bar
    @IBOutlet weak var familyImage: UIImageView!
    @IBOutlet weak var patientImage: UIImageView!
    @IBOutlet weak var batteryLevel: UIView!
    
    // end experience
    @IBOutlet weak var closeApp: UIButton!
    @IBOutlet weak var endBg: UIView!
    
    var familyIsPiloting: Bool = false
    var batteryWidthMax: Double = 18.0
    var batteryWidthCurrent: Double? = nil
    
    
    //////////// DEFINE PHASES IN AN ENUM ////////////
    
    enum Phase {
        case TakeOff
        case Waiting
        case Control
    }
    var phase = Phase.TakeOff
    
    //////////// VIEW DID LOAD ////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //////////// TOP BAR ////////////
        
        SocketConnectionManager.shared.returnConnections(familyImage: self.familyImage, patientImage: self.patientImage)

        
        //////////// DELEGATES & DATA SOURCE ////////////
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.collectionViewLayout.collectionView?.delegate = self
        
        //////////// STYLES ////////////
        
        // Collection view style
        self.collectionView.layer.backgroundColor = UIColor.clear.cgColor
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = flowLayout
        flowLayout.minimumLineSpacing = 0.0
        
        // Slider style
        self.takeOffSlider.customSliderTakeOff()
        
        // Waiting and take off phase background style
        self.viewBg.backgroundGradient(color: "blue")
        
        // EmergencyStop background
        self.viewBg2.backgroundGradient(color: "red")
        
        // Gimbal style
        self.gimbalSlider.customSliderGimbal()
        self.gimbal.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
        
        // End experience style
        self.closeApp.roundedCorner()
        self.closeApp.buttonOrange()

        self.endBg.backgroundGradient(color: "redGradient")
        
        // battery style
        SocketConnectionManager.shared.batteryLevel = {(pourcentageString) -> () in
            self.batteryWidthCurrent = (pourcentageString  as NSString).doubleValue
            self.batteryLevel.frame.size = CGSize(width: Double(self.batteryWidthCurrent! * self.batteryWidthMax / 100), height: 9.0)
            
            if self.batteryWidthCurrent! == 30.0 {
                self.batteryState(val: "Low")
            } else if self.batteryWidthCurrent! == 25.0 {
                self.batteryState(val: "End")
            } else if self.batteryWidthCurrent! > 30 {
                self.batteryState(val: "Ok")
            }
        }
        
        
        // view Bg Commands style
        self.viewBgCommands.backgroundGradient(color: "greyYellow")
        self.viewBgCommands.alpha = 0.0
        
        //  messages
        self.pilotageMsg.alpha = 0.0
        
        //////////// SET ELEMENTS ALPHA ON LOAD VIEW ////////////
        
        self.popFeeling.alpha = 0.0
        self.feelingBg.alpha = 0.0
        self.batteryMsg30.alpha = 0.0
        self.batteryMsg25.alpha = 0.0
        
        
        //////////// SET PHASE ACTIVE ON LOAD VIEW ////////////
        
        self.takeOffPhase()
//        self.waitingForTakeOffPhase()
//        self.controlPhase()
//        self.motorsStopped()
        
    
        //////////// TAPS FUNCTIONS ////////////
        
        let tripleTap = UITapGestureRecognizer(target: self, action: #selector(tripleTapAction))
        tripleTap.numberOfTapsRequired = 3
        view.addGestureRecognizer(tripleTap)
        
        let tapForward = UILongPressGestureRecognizer(target: self, action: #selector(goForward))
        tapForward.minimumPressDuration = 0
        self.forward.addGestureRecognizer(tapForward)
        
        let tapRight = UILongPressGestureRecognizer(target: self, action: #selector(goRight))
        tapRight.minimumPressDuration = 0
        self.right.addGestureRecognizer(tapRight)
        
        let tafLeft = UILongPressGestureRecognizer(target: self, action: #selector(goLeft))
        tafLeft.minimumPressDuration = 0
        self.left.addGestureRecognizer(tafLeft)
        
        let endTap = UITapGestureRecognizer(target: self, action: #selector(end))
        self.endTakeOffView.addGestureRecognizer(endTap)
        self.endCommands.addGestureRecognizer(endTap)
        
        
        //////////// SOCKET - GO TO CONTROL PHASE ////////////
        
        SocketIOManager.shared.nextStepCallback = {(step) -> () in
            if step == "3" {
                self.controlPhase()
                self.commandsEnabled(val: true)
            }
        }
        
        
        //////////// SOCKET - FEELINGS ////////////
        
        SocketIOManager.shared.feelingReceived = {(data) -> () in
            print("family send : \(data)" )
            //self.popFeeling.text = data
            self.popFeelingImg.image = UIImage(named: "pop_\(data)")
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.popFeeling.alpha = 1.0
            }, completion: nil)
            
            Timer.scheduledTimer(withTimeInterval: TimeInterval(4.0), repeats: false) { (t) in
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.popFeeling.alpha = 0.0
                }, completion: nil)
            }
        }
        
        SocketIOManager.shared.feelingFinished = {() -> () in
            print("feeling finished")
            if self.familyIsPiloting {
                self.commandsEnabled(val: false)
            } else {
                self.commandsEnabled(val: true)
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.feelingBg.alpha = 0.0
                }, completion: nil)
            }
        }
        
        
        //////////// SOCKET - PILOTING ////////////
        
        SocketIOManager.shared.didIAmPiloting = {(bool) -> () in
            self.commandsEnabled(val: bool)
            if !bool {
                self.familyIsPiloting = true
                
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.pilotageMsg.alpha = 1.0
                    self.viewBgCommands.alpha = 1.0
                }, completion: nil)
                
            } else {
                self.familyIsPiloting = false
                
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.pilotageMsg.alpha = 0.0
                    self.viewBgCommands.alpha = 0.0
                }, completion: nil)
            }
        }
        
        
        //////////// SOCKET - END ////////////
        
        SocketIOManager.shared.droneLanded = {() -> () in
            self.performSegue(withIdentifier: "goToEnd", sender: nil)
        }
        
        SocketIOManager.shared.experienceEnded = {() -> () in
            // display end message
        }
        
        SocketIOManager.shared.sparkMotorsAreOffCallback = {() -> () in
            self.stopMoves()
            self.commandsEnabled(val: false)
            self.motorsStopped()
        }
        
        
        //////////// SOCKET - VIDEO ////////////
        
        DispatchQueue.main.async {
            self.prev?.adjustViewSize()
            self.prev?.type = .fullWindow
        }

        SocketIOManager.shared.didReceiveDataCallback = {(data) -> () in
            self.feedVideoView(data: data)
        }
        
        startVideo()
        
    } // End viewDidLoad
    
    
    
    /////////////////////////////////////////
    //////////// VIDEO PREVIEWER ////////////
    /////////////////////////////////////////
    
    func startVideo() {
        prev?.setView(self.videoPreview)
        prev?.start()
        DispatchQueue.main.async {
            self.prev?.adjustViewSize()
            self.prev?.type = .fullWindow
        }
    }
    
    func feedVideoView(data:Data) {
        data.withUnsafeBytes{(bytes: UnsafePointer<UInt8>) in
            prev?.push(UnsafeMutablePointer(mutating: bytes), length: Int32(data.count))
        }
    }
    
    
    
    
    ////////////////////////////////////////////
    //////////// ENABLING FUNCTIONS ////////////
    ////////////////////////////////////////////
    
    func commandsEnabled(val: Bool) {
        self.feelingIsPlaying = !val
        if val{
            self.collectionView.alpha = 1.0
            self.wiz.alpha = 1.0
            
            self.forward.alpha = 1.0
            self.left.alpha = 1.0
            self.right.alpha = 1.0
            self.gimbal.alpha = 1.0
        } else {
            self.collectionView.alpha = 0.5
            self.wiz.alpha = 0.5
            
            self.forward.alpha = 0.5
            self.left.alpha = 0.5
            self.right.alpha = 0.5
            self.gimbal.alpha = 0.5
        }
    }
    
    func didEmergencyModeActived(val: Bool) {
        self.stopMotorsView.isHidden = !val
        switch phase {
            case .TakeOff:
                self.takeOffView.isHidden = val
            case .Waiting:
                self.takeOffView.isHidden = val
            case .Control:
                self.feelingView.isHidden = val
                self.commandsView.isHidden = val
        }
    }
    
    
    
    ////////////////////////////////
    //////////// PHASES ////////////
    ////////////////////////////////
    
    func takeOffPhase(){
        phase = .TakeOff
        self.feelingView.isHidden = true
        self.stopMotorsView.isHidden = true
        self.commandsView.isHidden = true
        self.takeOffView.isHidden = false
        self.waitingMessage1.isHidden = true
        self.waitingMessage2.isHidden = true
        self.validatedTakeOffLabel.isHidden = true
        self.motorsStoppedView.isHidden = true
    }
    
    func waitingForTakeOffPhase(){
        phase = .Waiting
        self.sliderView.isHidden = true
        self.feelingView.isHidden = true
        self.stopMotorsView.isHidden = true
        self.commandsView.isHidden = true
        self.takeOffView.isHidden = false
        self.waitingMessage1.isHidden = false
        self.waitingMessage2.isHidden = false
        self.validatedTakeOffLabel.isHidden = false
        self.motorsStoppedView.isHidden = true
    }
    
    func controlPhase(){
        phase = .Control
        self.sliderView.isHidden = true
        self.feelingView.isHidden = false
        self.stopMotorsView.isHidden = true
        self.commandsView.isHidden = false
        self.takeOffView.isHidden = true
        self.waitingMessage1.isHidden = true
        self.waitingMessage2.isHidden = true
        self.validatedTakeOffLabel.isHidden = true
        self.motorsStoppedView.isHidden = true
    }
    
    func batteryState(val: String){
        if val == "Ok"{
            self.batteryMsg30.isHidden = true
            self.batteryMsg25.isHidden = true
            
        } else if val == "Low" {
            self.batteryMsg30.isHidden = false
            self.batteryMsg25.isHidden = true
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.batteryMsg30.alpha = 1.0
            }, completion: nil)
            
        } else if val == "End" {
            self.batteryMsg30.isHidden = true
            self.batteryMsg25.isHidden = false
            self.pilotageMsg.isHidden = true
            self.commandsEnabled(val: false)
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.batteryMsg25.alpha = 1.0
                self.viewBgCommands.alpha = 1.0
            }, completion: nil)
        
        }
    }
    
    @IBOutlet weak var motorsStoppedView: UIView!
    func motorsStopped(){
        phase = .Control
        self.sliderView.isHidden = true
        self.feelingView.isHidden = true
        self.stopMotorsView.isHidden = true
        self.commandsView.isHidden = true
        self.takeOffView.isHidden = true
        self.waitingMessage1.isHidden = true
        self.waitingMessage2.isHidden = true
        self.validatedTakeOffLabel.isHidden = true
        self.motorsStoppedView.isHidden = false
    }
    
    
    //////////////////////////////////////////
    //////////// FONCTIONNALITIES ////////////
    //////////////////////////////////////////
    
    
    //////////// TAKE OFF SLIDER ////////////
    
    @IBAction func takeOff(_ sender: Any) {
        if self.takeOffSlider.value == self.takeOffSlider.maximumValue {
            SocketIOManager.shared.patientReady()
            self.waitingForTakeOffPhase()
        }
    }
    
    
    //////////// PILOTING ////////////
    
    @objc func goForward(gesture: UITapGestureRecognizer) {
        if !self.feelingIsPlaying {
            if gesture.state == .began {
                print("forward")
                SocketIOManager.shared.actionSparkDirectionHorizontal(direction: ".forward")
            } else if  gesture.state == .ended {
                print("stop")
                self.stopMoves()
            }
        }
    }
    
    @objc func goRight(gesture: UITapGestureRecognizer) {
        if !self.feelingIsPlaying {
            if gesture.state == .began {
                SocketIOManager.shared.actionSparkRotation(direction: ".clockwise")
            } else if  gesture.state == .ended {
                print("stop")
                self.stopMoves()
            }
        }
    }
    
    @objc func goLeft(gesture: UITapGestureRecognizer) {
        if !self.feelingIsPlaying {
            if gesture.state == .began {
                print("left")
                SocketIOManager.shared.actionSparkRotation(direction: ".counterClockwise")
            } else if  gesture.state == .ended {
                print("stop")
                self.stopMoves()
            }
        }
    }
    
    
    //////////// EMOTIONS ////////////
    
    func emotionIsPlaying() {
        self.commandsEnabled(val: false)
    }
    
    @IBAction func wiz(_ sender: Any) {
        if !self.feelingIsPlaying {
            print("wiz")
            self.emotionIsPlaying()
            self.feelingBg.image = UIImage(named: "halo_wiz")
            SocketIOManager.shared.actionSparkFeeling(feelingName: "wiz")
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.feelingBg.alpha = 1.0
            }, completion: nil)
        }
    }
    
    
    //////////// GIMBAL ////////////
    
    @IBAction func gimbalMoved(_ sender: Any) {
        let x = gimbalSlider.value
        let y = -180 * x + 90
        if y >= -90 && !self.familyIsPiloting {
            print("gimbal moved y: \(y)")
            SocketIOManager.shared.sendGimbalValue(y: y)
        }
    }
    
    
    //////////// STOP ////////////
    
    func stopMoves() {
        print("stop")
        SocketIOManager.shared.actionSparkEvent(event: ".stop")
    }
    
    @IBAction func stopMotorYes(_ sender: Any) {
        SocketIOManager.shared.actionSparkEvent(event: ".emergencyStop")
    }
    
    @IBAction func stopMotorNo(_ sender: Any) {
        self.didEmergencyModeActived(val: false)
    }
    
    @objc func tripleTapAction() {
        print("Emergency stop")
        self.didEmergencyModeActived(val: true)
    }
    
    
    //////////// END ////////////
    
    @objc func end() {
        print("end experience")
        self.stopMoves()
        self.pilotageMsg.isHidden = true
        SocketIOManager.shared.endExperience()
        
    }
    
    @IBAction func closeApp(_ sender: Any) {
        self.performSegue(withIdentifier: "goBackHome", sender: nil)
        UIControl().sendAction(#selector(NSXPCConnection.suspend),
                               to: UIApplication.shared, for: nil)
    }
    
    @IBAction func goBackToHome(_ sender: Any) {
        self.performSegue(withIdentifier: "goBackHome", sender: nil)
    }
}




////////////////////////////////////////////////
//////////// CONTROLLERS EXTENTIONS ////////////
////////////////////////////////////////////////

extension ControlViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let feelingName = "\(feelingsList[indexPath.item][0])"
        if !self.feelingIsPlaying {
            print("\(self.feelingsList[indexPath.item][0])")
            
            SocketIOManager.shared.actionSparkFeeling(feelingName: feelingName)
            self.commandsEnabled(val: false)
            self.feelingBg.image = UIImage(named: "halo_\(self.feelingsList[indexPath.item][0])")
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.feelingBg.alpha = 1.0
            }, completion: nil)
        }
    }
}

extension ControlViewController:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 5.5
        let height = collectionView.frame.height
        return CGSize(width: width, height: height)
    }
}

extension ControlViewController:UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feelingsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeelingCell", for: indexPath) as! FeelingViewCell
        
        cell.icon?.image = feelingsList[indexPath.item][1] as? UIImage
        
        return cell
    }
}
